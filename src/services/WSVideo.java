package services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;

import entity.Video;
import persistence.VideoDao;

@Path("/video")
public class WSVideo {

	//http://localhost:8080/ListaService/webservice/video/gravar/Video4/Aula4/www.youtube.com/embed
	@Path("/gravar/{titulo}/{subtitulo}/{link}/{linkimagem}")
	@GET
	@Produces("text/plain")
	public String gravar(@PathParam("titulo") String titulo, @PathParam("subtitulo") String subtitulo,
			@PathParam("'link'") String link, @PathParam("'linkimagem'") String linkimagem) {

		VideoDao vd = new VideoDao();
		Video v = new Video(null, titulo, subtitulo, link, linkimagem);
		String msg = "";

		try {
			vd.create(v);
			msg = "video adicionado";
		} catch (Exception e) {
			msg = "Error " + e.getMessage();
		}

		return msg;
	}

	
	//http://localhost:8080/ListaService/webservice/video/listar
	@Path("/listar")
	@GET
	@Produces("application/json")
	//@Produces("text/plain")
	public String listar() {
		try {
			String inicio = "{\"android\":";
			String fim = "}";
			return inicio + new Gson().toJson(new VideoDao().findAll())+fim;
			
			
		} catch (Exception ex) {
			return "Error :" + ex.getMessage();
		}
	}

}
