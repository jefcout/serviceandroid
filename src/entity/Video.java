package entity;

public class Video {

	private Integer idVideo;
	private String titulo;
	private String subtitulo;
	private String link;
	private String linkimagem;
	
	public Video() {
		// TODO Auto-generated constructor stub
	}

	public Video(Integer idVideo, String titulo, String subtitulo, String link, String linkimagem) {
		super();
		this.idVideo = idVideo;
		this.titulo = titulo;
		this.subtitulo = subtitulo;
		this.link = link;
		this.linkimagem = linkimagem;
	}

	@Override
	public String toString() {
		return "Video [idVideo=" + idVideo + ", titulo=" + titulo + ", subtitulo=" + subtitulo + ", link=" + link
				+ ", linkimagem=" + linkimagem + "]";
	}

	public Integer getIdVideo() {
		return idVideo;
	}

	public void setIdVideo(Integer idVideo) {
		this.idVideo = idVideo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getSubtitulo() {
		return subtitulo;
	}

	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLinkimagem() {
		return linkimagem;
	}

	public void setLinkimagem(String linkimagem) {
		this.linkimagem = linkimagem;
	}

	
}
