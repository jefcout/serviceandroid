package persistence;

import java.util.ArrayList;
import java.util.List;

import entity.Video;

public class VideoDao extends Dao {

	public void create(Video video) throws Exception {
		open();
		stmt = con.prepareStatement("insert into videos values (null,?,?,?,?)");
		stmt.setString(1, video.getTitulo());
		stmt.setString(2, video.getSubtitulo());
		stmt.setString(3, video.getLink());
		stmt.setString(4, video.getLinkimagem());
		stmt.execute();
		stmt.close();
		close();
	}


    public List<Video> findAll() throws Exception {
        open();
        stmt = con.prepareStatement("select * from videos");
        List<Video> lista = new ArrayList<Video>();
        rs = stmt.executeQuery();
        while (rs.next()) {
            Video v = null;
            v = new Video();
            v.setIdVideo(rs.getInt(1));
            v.setTitulo(rs.getString(2));
            v.setSubtitulo(rs.getString(3));
            v.setLink(rs.getString(4));
            v.setLinkimagem(rs.getString(5));
            lista.add(v);
        }
        close();
        return lista;
    }
	public static void main(String[] args) {
		Video video = new Video(null, "Video 3", "Aula 3", "https://www.youtube.com/embed/3EEAYXLTTDM","https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcR5IwWIfNLK4NtDLEtN25-aaIxK-vMqBHq2t-MzTPuhuPOOAGjIfTgk7I0");

		VideoDao vd = new VideoDao();
		try {
			 //vd.create(video);
			 //System.out.println("Video " + video.getTitulo()+" Adicionado");
			System.out.println(vd.findAll());
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
